# NHR_Workshop_Okt23

## Data Information

The database "training_SiO2.pckl.gzip" is a subpart of the database from npj Computational Materials 8, 90 (2022).
The "large_sio2.xyz" structure is taken from the same paper. 
